//
//  SecondViewController.swift
//  ExpenseTrackApp6
//
//  Created by Matt DeBoer on 2/10/18.
//  Copyright © 2018 Matt DeBoer. All rights reserved.
//

import UIKit
import CoreData

var object:Dictionary = [String:Any]()

class SecondViewController: UIViewController {
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var textField1: UITextField!
    
    @IBOutlet weak var textField2: UITextField!
    @IBOutlet weak var textField3: UITextField!
    
    @IBOutlet weak var saveButton: UIButton!
    
    
    var selectedExpense:Expense?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let expense = selectedExpense
        {
            textField.text=selectedExpense?.locationName
            textField1.text=selectedExpense?.dat
            textField2.text=selectedExpense?.cost
            textField3.text=selectedExpense?.notes
            saveButton.setTitle("Update", for: .normal)
        }
    }
    // Do any additional setup after loading the view.
    
    
    @IBAction func savePressed(_ sender: UIButton) {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate,
            let lN = textField.text, let dat=textField1.text,
            let cost = textField2.text, let note = textField3.text else {
                return
        }
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        if let sel=selectedExpense
        {
            sel.setValue(lN, forKeyPath: "locationName")
            sel.setValue(dat, forKeyPath: "dat")
            sel.setValue(cost, forKeyPath: "cost")
            sel.setValue(note, forKeyPath: "notes")
        }
        else{
            
            // 2
            let entity = NSEntityDescription.entity(forEntityName: "Expense",in: managedContext)!
            //
            let expense = NSManagedObject(entity: entity, insertInto: managedContext)
            
            expense.setValue(lN, forKeyPath: "locationName")
            expense.setValue(dat, forKeyPath: "dat")
            expense.setValue(cost, forKeyPath: "cost")
            expense.setValue(note, forKeyPath: "notes")
        }
        do {
            try managedContext.save()
            self.navigationController?.popViewController(animated: true)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
