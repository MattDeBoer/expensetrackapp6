//
//  TCell.swift
//  ExpenseTrackApp6
//
//  Created by Matt DeBoer on 2/11/18.
//  Copyright © 2018 Matt DeBoer. All rights reserved.
//

import UIKit

class TCell: UITableViewCell {
    
    @IBOutlet weak var loc: UILabel!
    
    @IBOutlet weak var dat: UILabel!
    
    @IBOutlet weak var cost: UILabel!
    
    @IBOutlet weak var note: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

