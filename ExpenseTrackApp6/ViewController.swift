//
//  ViewController.swift
//  ExpenseTrackApp6
//
//  Created by Matt DeBoer on 2/10/18.
//  Copyright © 2018 Matt DeBoer. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tabla: UITableView!
    var tracker: [Expense] = []
    var selectedExpense:Expense?
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        // 1
        let managedContext =
            appDelegate.persistentContainer.viewContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "Expense",in: managedContext)!
        //
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Expense")
        //3
        do {
            
            let searchResults = try managedContext.fetch(fetchRequest) as! [Expense]
            if(searchResults.count>0)
            {
                tracker=searchResults
                tabla.reloadData()
            }
            else
            {
                tracker=[]
            }
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tracker.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:TCell = tableView.dequeueReusableCell(withIdentifier: "t") as! TCell
        cell.loc.text=tracker[indexPath.row].locationName
        cell.dat.text=tracker[indexPath.row].dat
        cell.cost.text=tracker[indexPath.row].cost
        cell.note.text=tracker[indexPath.row].notes
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        selectedExpense = tracker[indexPath.row]
        self.performSegue(withIdentifier: "makingTrans", sender: tracker[indexPath.row])
        
    }
        
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier=="makingTrans")
        {
            let secondVC = segue.destination as! SecondViewController
            secondVC.selectedExpense=selectedExpense
        }
        
    }
}

